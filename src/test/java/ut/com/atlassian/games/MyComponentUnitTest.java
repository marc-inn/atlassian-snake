package ut.com.atlassian.games;

import org.junit.Test;
import com.atlassian.games.MyPluginComponent;
import com.atlassian.games.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}