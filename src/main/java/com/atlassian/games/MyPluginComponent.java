package com.atlassian.games;

public interface MyPluginComponent
{
    String getName();
}